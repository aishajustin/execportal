
@extends('layouts.appAdmin')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href=""><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                <li class="breadcrumb-item active">dashboard</li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>



<div class="container-fluid">

  <div class="row clearfix">
                <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon traffic">
                  <div class="body">
                      <h6>Organisations</h6>
                      <h2>20 <small class="info"></small></h2>
                      <small>number of Organisations registered</small>
                      <div class="progress">
                          <div class="progress-bar l-amber" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"></div>
                      </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon sales">
                  <div class="body">
                      <h6>Executives</h6>
                      <h2>12<small class="info"></small></h2>
                      <small>number of executives registered</small>
                      <div class="progress">
                          <div class="progress-bar l-blue" role="progressbar" aria-valuenow="38" aria-valuemin="0" aria-valuemax="100" style="width: 38%;"></div>
                      </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon email">
                  <div class="body">
                      <h6>Interviews</h6>
                      <h2>39 <small class="info">of 100</small></h2>
                      <small>Total number of interviews held</small>
                      <div class="progress">
                          <div class="progress-bar l-purple" role="progressbar" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100" style="width: 39%;"></div>
                      </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                <div class="card widget_2 big_icon domains">
                  <div class="body">
                      <h6>Jobs Secured</h6>
                      <h2>8 <small class="info"></small></h2>
                      <small>Number of Executives hired using Exec-Portal </small>
                      <div class="progress">
                          <div class="progress-bar l-green" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%;"></div>
                      </div>
                  </div>
                </div>
                </div>
                </div>



                <! ------------------------- new actions notifications ------------------->


                <div class="row clearfix js-sweetalert">
                 <div class="col-md-12">
                     <div class="card">

                       <div class="alert alert-info">
                                <strong>Heads up!</strong> The follwowing has taken place since your last visit.....
                            </div>


                         <div class="table-responsive">
                             <table class="table table-hover mb-0 c_table">

                                 <tr>
                                     <td><button class="btn btn-raised btn-primary waves-effect" data-type="we-set-buttons">VIEW</button></td>
                                     <td><span>New organisation registered on our Portal, Their account is waiting to be approved</span></td>
                                 </tr>
                                 <tr>
                                     <td><button class="btn btn-raised btn-primary waves-effect" data-type="basic">VIEW ALL</button></td>
                                     <td><span>New executives registered</span></td>
                                 </tr>
                             </table>
                         </div>
                     </div>
                 </div>
             </div>







</div>

@endsection

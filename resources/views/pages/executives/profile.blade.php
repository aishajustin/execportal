@extends('layouts.appExecutives')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/executivesIndex')}}"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                <li class="breadcrumb-item active">my profile </li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>

<div class="container-fluid">



  <div class="body_scroll">

        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12">
                    <div class="card mcard_3">
                        <div class="body">
                            <a href="profile.html"><img src="assets/images/profile_av.jpg" class="rounded-circle shadow " alt="profile-image"></a>
                            <h4 class="m-t-10">Executive Aisha</h4>
                            <div class="row">
                                <div class="col-12">

                                    <p class="text-muted">795 Folsom Ave, Suite 600 San Francisco, CADGE 94107</p>
                                </div>
                                <div class="col-4">
                                    <small>Views</small>
                                    <h5>200</h5>
                                </div>
                                <div class="col-4">
                                    <small>Profile Request</small>
                                    <h5>15</h5>
                                </div>
                                <div class="col-4">
                                      <small>interviews</small>
                                    <h5>3</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">

                            <hr>
                            <small class="text-muted">Profile Status: </small>

                            <hr>
                            <ul class="list-unstyled">
                                <li>
                                    <div>Personal Information</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-blue " role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%"> <span class="sr-only">62% Complete</span> </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Qualifications</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-green " role="progressbar" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%"> <span class="sr-only">87% Complete</span> </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Experience</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-amber" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%"> <span class="sr-only">32% Complete</span> </div>
                                    </div>
                                </li>
                                <li>
                                    <div>Video Trailer</div>
                                    <div class="progress m-b-20">
                                        <div class="progress-bar l-blush" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%"> <span class="sr-only">0% Complete</span> </div>
                                    </div>
                                </li>
                            </ul>
                            <span>Complete your profile by upaloding all the required content and submitting all the data so that you have a higher chance of being found by Organisations.</span>
                        </div>
                    </div>
                </div>
              </div>
</div>
@endsection

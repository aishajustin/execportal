@extends('layouts.appExecutives')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/executivesIndex')}}"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                <li class="breadcrumb-item active">my Job Applications </li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>

<div class="container-fluid">

  <div class="body_scroll">

<div class="container-fluid">
 <div class="row clearfix">
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card state_w1">
             <div class="body d-flex justify-content-between">
                 <div>
                     <h5>0</h5>
                     <span>Total Applications</span>
                 </div>
                 <div class="sparkline" data-type="bar" data-width="97%" data-height="55px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#FFC107">5,2,3,7,6,4,8,1</div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card state_w1">
             <div class="body d-flex justify-content-between">
                 <div>
                     <h5>0</h5>
                     <span>Pending</span>
                 </div>
                 <div class="sparkline" data-type="bar" data-width="97%" data-height="55px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#46b6fe">8,2,6,5,1,4,4,3</div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card state_w1">
             <div class="body d-flex justify-content-between">
                 <div>
                     <h5>0</h5>
                     <span>Responded</span>
                 </div>
                 <div class="sparkline" data-type="bar" data-width="97%" data-height="55px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#ee2558">4,4,3,9,2,1,5,7</div>
             </div>
         </div>
     </div>
     <div class="col-lg-3 col-md-6 col-sm-6">
         <div class="card state_w1">
             <div class="body d-flex justify-content-between">
                 <div>
                     <h5>Accepted Interview</h5>
                     <span>Resolve</span>
                 </div>
                 <div class="sparkline" data-type="bar" data-width="97%" data-height="55px" data-bar-Width="3" data-bar-Spacing="5" data-bar-Color="#04BE5B">7,5,3,8,4,6,2,9</div>
             </div>
         </div>
     </div>
 </div>
 <div class="row clearfix">
     <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="card project_list">
             <div class="table-responsive">
                 <table class="table table-hover c_table">
                     <thead>
                         <tr>
                             <th>#</th>
                             <th>Job title</th>
                             <th>Organisation</th>
                             <th>Job Description</th>
                             <th>Date Posted</th>
                             <th>Comments</th>
                             <th>Status</th>
                         </tr>
                     </thead>
                     <tbody>
                         <tr>
                             <td><strong>A2586</strong></td>
                             <td><a href="applicationDetail.html" title="">Lucid Side Menu Open OnClick</a></td>
                             <td>Lucid Admin</td>
                             <td>Tim Hank</td>
                             <td>02 Jan 2019</td>
                             <td>Maryam</td>
                             <td><span class="badge badge-warning">In Progress</span></td>
                         </tr>
                         <tr>
                             <td><strong>A4578</strong></td>
                             <td><a href="applicationDetail.html" title="">Update chart library</a></td>
                             <td>Alpino Bootstrap</td>
                             <td>Tim Hank</td>
                             <td>04 Jan 2019</td>
                             <td>Hossein</td>
                             <td><span class="badge badge-warning">In Progress</span></td>
                         </tr>


                     </tbody>
                 </table>
             </div>
             <ul class="pagination pagination-primary mt-4">
                 <li class="page-item active"><a class="page-link" href="javascript:void(0);">1</a></li>
                 <li class="page-item"><a class="page-link" href="javascript:void(0);">2</a></li>
                 <li class="page-item"><a class="page-link" href="javascript:void(0);">3</a></li>
             </ul>
         </div>
     </div>
 </div>
</div>
</div>







</div>
@endsection

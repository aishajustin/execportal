@extends('layouts.appExecutives')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url('/executivesIndex')}}"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                <li class="breadcrumb-item active">View Job Applicatons </li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>

<div class="container-fluid">

  <div class="col-xl-8 col-lg-12 col-md-12">
  <div class="row clearfix">

      <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <a href="">
                        <img class="img-fluid img-thumbnail" src="" alt="">
                    </a>
                    <h5 class="mt-3">COF - ORGANISATION A</h5>
                    <p>COF needed for Org A with 3-4 years exprirence in E.</p>
                    <div class="d-flex justify-content-between">
                        <a href="jobDetail.html" class="btn btn-info">Preview</a>
                        <a href="jobDetail.html" class="btn btn-danger">Apply</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="body">
                    <a href="">
                        <img class="img-fluid img-thumbnail" src="" alt="">
                    </a>
                    <h5 class="mt-3">COF - ORGANISATION A</h5>
                    <p>COF needed for Org A with 3-4 years exprirence in E.</p>
                    <div class="d-flex justify-content-between">
                        <a href="jobDetail.html" class="btn btn-info">Preview</a>
                        <a href="jobDetail.html" class="btn btn-danger">Apply</a>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <a href="">
                        <img class="img-fluid img-thumbnail" src="" alt="">
                    </a>
                    <h5 class="mt-3">ceo - ORGANISATION b</h5>
                    <p>COF needed for Org A with 3-4 years exprirence in E.</p>
                    <div class="d-flex justify-content-between">
                        <a href="" class="btn btn-info">Preview</a>
                        <a href="" class="btn btn-danger">Apply</a>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="body">
                    <a href="">
                        <img class="img-fluid img-thumbnail" src="" alt="">
                    </a>
                    <h5 class="mt-3">COF - ORGANISATION A</h5>
                    <p>COF needed for Org A with 3-4 years exprirence in E.</p>
                    <div class="d-flex justify-content-between">
                        <a href="" class="btn btn-info">Preview</a>
                        <a href="" class="btn btn-danger">Apply</a>
                    </div>
                </div>
            </div>
          </div>
        </div>
</div>



</div>
@endsection

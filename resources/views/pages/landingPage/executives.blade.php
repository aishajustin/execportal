<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.png" type="image/png">
	<title>Exec Portal</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets_landing_page/css/bootstrap.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/linericon/style.css">
	<link rel="stylesheet" href="assets_landing_page/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="assets_landing_page/css/magnific-popup.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="assets_landing_page/css/style.css">
</head>

    <body>

			<!--================Header Menu Area =================-->
			<header class="header_area">
				<div class="main_menu">
					<nav class="navbar navbar-expand-lg navbar-light">
						<div class="container">
							<!-- Brand and toggle get grouped for better mobile display -->
							<a class="navbar-brand logo_h" href="index.html">Exec-Portal</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
							 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
								<ul class="nav navbar-nav menu_nav justify-content-center">
									<li class="nav-item "><a class="nav-link" href="{{ url('/') }}">Home</a></li>
									<li class="nav-item "><a class="nav-link" href="{{ url('/org') }}">Organisations</a></li>
									<li class="nav-item active"><a class="nav-link" href="{{ url('/exe') }}">Executives</a></li>

									<li class="nav-item"><a class="nav-link" href="{{ url('/contact') }}">Contact</a></li>
										<li class="nav-item"><a class="nav-link" href="{{ url('/signin') }}">Login</a></li>
								</ul>
								<ul class="nav navbar-nav navbar-right">
									<li class="nav-item"><a href="{{ url('/signup') }}" class="primary_btn text-uppercase">Sign Up</a></li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</header>
			<!--================Header Menu Area =================-->

        <!--================ Start Element Banner Area =================-->
    <section class="banner_area">
      <div class="banner_inner d-flex align-items-center">
        <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
        <div class="container">
          <div class="banner_content text-left">
            <div class="page_link">
              <a href="index.html">Home</a>
              <a href="executives.html">executives</a>
            </div>
            <h2>executive candidates</h2>
          </div>
        </div>
      </div>
    </section>
    <!--================ End Element Banner Area =================-->


        <!-- Start Sample Area -->
      <section class="sample-text-area">
        <div class="container">

          <div class="col-lg-12 col-sm-6 mt-sm-30 typo-sec">
								<h3 class="mb-20 title_color">How it works for Executives</h3>
								<div class="">
									<ul class="unordered-list">
										<li>	The system registration pages will guide you through a smart profile process from beginning to finish.</li>
										<li>Save time! Relax and think that about 800 companies will see your profile… Some documentations will be required for uploading as the candidate conclude the registration.</li>
										<li>All that is left to do is to wait to be contacted by and employer for an interview!!!</li>

								</div>
							</div>


          <form class="row contact_form" >
            <div class="col-md-12 text-right">
                <a href="{{('/signup')}}">    <button type="submit" value="submit" class="primary_btn"><span>Open an Executive Account</span></button> </a>
            </div>
          </form>
        </div>





      </section>
      <!-- End Sample Area -->

      <!--================Footer Area =================-->
    	<footer class="footer_area">
    		<div class="container">
    			<div class="row footer_inner">
    				<div class="col-lg-5 col-sm-6">
    					<aside class="f_widget ab_widget">
    						<div class="f_title">
    							<h3>About Me</h3>
    						</div>
    						<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | System made by <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://ecowebplus.com" target="_blank">EcoWebPlus</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
    					</aside>
    				</div>
    				<div class="col-lg-5 col-sm-6">
    					<aside class="f_widget news_widget">
    						<div class="f_title">
    							<h3>Newsletter</h3>
    						</div>
    						<p>Stay updated with our latest trends</p>
    						<div id="mc_embed_signup">
    							<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
    							 method="get" class="subscribe_form relative">
    								<div class="input-group d-flex flex-row">
    									<input name="EMAIL" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '"
    									 required="" type="email">
    									<button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>
    								</div>
    								<div class="mt-10 info"></div>
    							</form>
    						</div>
    					</aside>
    				</div>

    			</div>
    		</div>
    	</footer>
    	<!--================End Footer Area =================-->



      	<!-- Optional JavaScript -->
      	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
      	<script src="assets_landing_page/js/jquery-3.2.1.min.js"></script>
      	<script src="assets_landing_page/js/popper.js"></script>
      	<script src="assets_landing_page/js/bootstrap.min.js"></script>
      	<script src="assets_landing_page/js/stellar.js"></script>
      	<script src="assets_landing_page/js/jquery.magnific-popup.min.js"></script>
      	<script src="assets_landing_page/vendors/nice-select/js/jquery.nice-select.min.js"></script>
      	<script src="assets_landing_page/vendors/isotope/imagesloaded.pkgd.min.js"></script>
      	<script src="assets_landing_page/vendors/isotope/isotope-min.js"></script>
      	<script src="assets_landing_page/vendors/owl-carousel/owl.carousel.min.js"></script>
      	<script src="assets_landing_page/js/jquery.ajaxchimp.min.js"></script>
      	<script src="assets_landing_page/vendors/counter-up/jquery.waypoints.min.js"></script>
      	<script src="assets_landing_page/vendors/counter-up/jquery.counterup.min.js"></script>
      	<script src="assets_landing_pagejs/mail-script.js"></script>
      	<!--gmaps Js-->
      	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
      	<script src="assets_landing_page/js/gmaps.min.js"></script>
      	<script src="assets_landing_page/js/theme.js"></script>
      </body>

      </html>

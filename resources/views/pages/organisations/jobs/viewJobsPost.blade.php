
@extends('layouts.appOrg')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href=""><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                <li class="breadcrumb-item active">View Jobs Post</li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>


<div class="container-fluid">
  <div class="row clearfix">
<div class="col-lg-12">
    <div class="card">
        <div class="table-responsive">
            <table class="table table-hover mb-0 c_list c_table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th data-breakpoints="xs">Title</th>
                        <th data-breakpoints="xs sm md">Description</th>
                        <th data-breakpoints="xs sm md">Date posted</th>
                        <th data-breakpoints="xs">Executives</th>
                        <th data-breakpoints="xs">Experience</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="checkbox">
                                <input id="delete_2" type="checkbox">
                                <label for="delete_2">&nbsp;</label>
                            </div>
                        </td>
                        <td>
                            <p class="c_name">02</p>
                        </td>
                        <td>
                            <p class="c_name">Chief Exxecutive Officer</p>
                        </td>
                        <td>
                            <span class="phone"><i class="zmdi zmdi-whatsapp mr-2"></i>New executive needed for our MAJ Holdings starting end of month</span>
                        </td>
                        <td>
                            <span class="email"><a href="javascript:void(0);" title="">info@maj.co.za</a></span>
                        </td>
                        <td>
                            <address><i class="zmdi zmdi-pin"></i>CEO, COO, CA </address>
                        </td>
                        <td>
                            <address><i class="zmdi zmdi-pin"></i> 3-4 + years</address>
                        </td>
                        <td>
                            <button class="btn btn-primary btn-sm"><i class="zmdi zmdi-edit"></i></button>
                            <button class="btn btn-danger btn-sm"><i class="zmdi zmdi-delete"></i></button>
                        </td>
                    </tr>


                </tbody>
            </table>
        </div>
    </div>
</div>

</div>


@endsection


@extends('layouts.appAdmin')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                  <li class="breadcrumb-item active">View Organisations</li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>



<div class="container-fluid">
  <div class="col-lg-12">
           <div class="card">
               <div class="header">
                   <h2><strong>ORGANISATIONS</strong> list </h2>
                   <ul class="header-dropdown">
                       <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                           <ul class="dropdown-menu dropdown-menu-right slideUp">
                               <li><a href="javascript:void(0);">Action</a></li>
                               <li><a href="javascript:void(0);">Another action</a></li>
                               <li><a href="javascript:void(0);">Something else</a></li>
                           </ul>
                       </li>
                       <li class="remove">
                           <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                       </li>
                   </ul>
               </div>
               <div class="body">
                   <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                           <thead>
                               <tr>
                                   <th>organisation ID</th>
                                   <th>organisation Name</th>
                                   <th>User Name</th>
                                   <th>Work Email</th>
                                   <th>Contact Number</th>
                                   <th>Address</th>
                               </tr>
                           </thead>
                           <tfoot>
                               <tr>
                                 <th>organisation ID</th>
                                 <th>organisation Name</th>
                                 <th>User Name</th>
                                 <th>Work Email</th>
                                 <th>Contact Number</th>
                                 <th>Address</th>
                               </tr>
                           </tfoot>
                           <tbody>


                               <tr>
                                   <td>Michael Bruce</td>
                                   <td>Javascript Developer</td>
                                   <td>Singapore</td>
                                   <td>29</td>
                                   <td>2011/06/27</td>
                                   <td>$183,000</td>
                               </tr>

                           </tbody>
                       </table>
                   </div>
               </div>
           </div>
       </div>

       <a href="{{url('/addOrganisation')}}"><button class="btn btn-raised btn-primary waves-effect" data-type="basic">ADD NEW ORGANISATION</button></a>


</div>


@endsection

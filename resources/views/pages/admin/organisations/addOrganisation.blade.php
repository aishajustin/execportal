
@extends('layouts.appAdmin')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                  <li class="breadcrumb-item active">Add an Organisation</li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>



<div class="container-fluid">
        <div class="card">
                  <div class="body">
                      <h2 class="card-inside-title">Add a new Organisation</h2>
                      <div class="row clearfix">
                          <div class="col-sm-12">

                             <div class="form-group mb-6">
                               <label   for="name" >Organisation Name </label>
                                      <input id="orgName"   name="name" type="text"   class="form-control validate"  required  />
                             </div>

                             <div class="form-group mb-6"><label for="name"  >Organisation User Name</label>
                               <input id="orgUserName"   name="name" type="text"   class="form-control validate"  required  />
                            </div>

                                <div class="form-group mb-6"><label for="name"  >Organisation Email</label>
                                  <input id="workEmail"   name="name" type="text"   class="form-control validate"  required  />
                               </div>

                               <div class="form-group mb-6"><label for="name"  >Organisation Number</label>
                                 <input id="orgNumber"   name="number" type="text"   class="form-control validate"  required  />
                              </div>

                              <div class="form-group mb-6"><label for="name"  >Organisation Address</label>
                                <input id="orgAdress"   name="number" type="text"   class="form-control validate"  required  />
                             </div
                         </div>



                   </div>

                   <div class="col-12">
                     <button type="submit" class="btn btn-primary btn-block text-uppercase">Add Organisation Now</button>
                   </div>
                          </div>
                      </div>
          </div>





</div>


@endsection


@extends('layouts.appAdmin')

@section('content')

<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Dashboard</h2>
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> EXEC-Portal</a></li>
                  <li class="breadcrumb-item active">View Executives</li>
            </ul>
            <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
        </div>
    </div>
</div>



<div class="container-fluid">

  <div class="row clearfix">
           <div class="col-lg-12">
               <div class="card">
                   <div class="header">
                       <h2><strong>LIST OF EXECUTIVES</strong>  </h2>
                       <ul class="header-dropdown">

                           <li class="remove">
                               <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                           </li>
                       </ul>
                   </div>
                   <div class="body">
                       <div class="table-responsive">
                           <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                               <thead>
                                   <tr>
                                       <th>Executive ID</th>
                                       <th> Name</th>
                                       <th>Surname</th>
                                       <th> Email</th>
                                       <th>Contact Number</th>
                                       <th>Province</th>
                                       <th>Occupation</th>
                                       <th>Employed?</th>
                                       <th>Action?</th>
                                   </tr>
                               </thead>
                               <tfoot>
                                   <tr>
                                     <th>Executive ID</th>
                                     <th> Name</th>
                                     <th>Surname</th>
                                     <th> Email</th>
                                     <th>Contact Number</th>
                                     <th>Province</th>
                                     <th>Occupation</th>
                                     <th>Employed?</th>
                                     <th>Action?</th>
                                   </tr>
                               </tfoot>
                               <tbody>


                                   <tr>
                                       <td>Michael Bruce</td>
                                       <td>Javascript Developer</td>
                                       <td>Singapore</td>
                                       <td>29</td>
                                       <td>2011/06/27</td>
                                       <td>$183,000</td>
                                   </tr>

                               </tbody>
                           </table>
                       </div>
                   </div>
               </div>
           </div>

       </div>

</div>


@endsection

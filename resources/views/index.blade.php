<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="img/favicon.png" type="image/png">
	<title>Exec Portal</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="assets_landing_page/css/bootstrap.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/linericon/style.css">
	<link rel="stylesheet" href="assets_landing_page/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="assets_landing_page/css/magnific-popup.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="assets_landing_page/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="assets_landing_page/css/style.css">
</head>

<body>

  <!--================Header Menu Area =================-->
  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <a class="navbar-brand logo_h" href="index.html">Exec-Portal</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
           aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-center">
              <li class="nav-item active"><a class="nav-link" href="{{ url('/') }}">Home</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ url('/org') }}">Organisations</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ url('/exe') }}">Executives</a></li>

              <li class="nav-item"><a class="nav-link" href="{{ url('/contact') }}">Contact</a></li>
                <li class="nav-item"><a class="nav-link" href="{{ url('/signin') }}">Login</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="nav-item"><a href="{{ url('/signup') }}" class="primary_btn text-uppercase">Sign Up</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="home_banner_area">
		<div class="banner_inner">
			<div class="container">
				<div class="row">
					<div class="col-lg-5">
						<div class="banner_content">
							<h2>
								EXECUTIVE PORTAL<br>
							</h2>
							<p>
								A platform that brings Organisations and Executive Candidates together to a space to connect  and bring innovation to businesses.
							</p>
							<div class="d-flex align-items-center">
								<a class="primary_btn" href="{{ url('/signup') }}"><span>Get Started</span></a>
								<a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?timetinue=2&v=J9YzcEe29d0">
									<span></span>
								</a>
								<div class="watch_video text-uppercase">
									watch the video
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-7">
						<div class="home_right_img">
							<img class="img-fluid"src="assets_landing_page/img/banner/home-right.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->

	<!--================Start Features Area =================-->
	<section class="section_gap features_area">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<div class="main_title">
						<p class="top_title">Exclusive Stunning Features</p>
						<h2>Welcome to our smart platform! </h2>
						<p>We at EXECUTIVE PORTAL proud ourselves in leading the world of innovation and smart solutions. We have launched this system as a smart solution for a specific level of employment: the Executive! </p>
						<a href="#" class="primary_btn"><span>Register now to get Started!</span></a>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col-lg-6">
					<div class="left_features">
						<img class="img-fluid" src="assets_landing_page/img/f-img.png" alt="">
					</div>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<!-- single features -->
					<div class="single_feature">
						<div class="feature_head">
							<span class="lnr lnr-screen"></span>
							<h4>Executive Candidates and Organisations</h4>
						</div>
						<div class="feature_content">
							<p style="font-size:18px">Thinking of smart solutions, this platform targets Employers and Executive candidates; to bring them together for employers to fill in their demanding need for CFO’s, CA’s to name but few. </p>
						</div>
					</div>
					<!-- single features -->
					<div class="single_feature">
						<div class="feature_head">
							<span class="lnr lnr-screen"></span>
							<h4>Stunning Visuals</h4>
						</div>
						<div class="feature_content">
							<p style="font-size:19px">It is exhilarating for candidates to think that over 800 Employers will be shown their profile while we will ensure that the candidate has a well-crafted profile that stands out of the crowd.    </p>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>
	<!--================End Features Area =================-->

	<!--================Recent Update Area =================-->
	<section class="recent_update_area">
		<div class="container">
			<div class="recent_update_inner">
				<ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
							<span class="lnr lnr-screen"></span>
							<h6>System Admin</h6>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
						 aria-selected="false">
						 <span class="lnr lnr-screen"></span>
						 <h6>Organisations</h6>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact"
						 aria-selected="false">
						 <span class="lnr lnr-screen"></span>
						 <h6>Executives</h6>
						</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">System Admin Role</p>
									<h3>We Believe that <br /> Inner beauty Lasts Long</h3>
									<p>The Exec-Portal has a crucial role which is played by the system admin admin who is responcible for revifying the validity of an Organisation   and the integrity of each executive that is registered on our system </p>
									<a class="primary_btn" href="{{ url('/signup') }}"><span>Learn More</span></a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="chart_img">
									<img class="img-fluid" src="assets_landing_page/img/chart.png" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">Organisations Role</p>
									<h3>A registered Organisation will have access to thousands of candidates </h3>
									<p>While social media platforms connect you to myriads of friends and family members, our system links you up to an unlimited choice of great and smart CFO’s, CTO’s and CA’s to name but a few...

</p>
									<a class="primary_btn" href="{{ url('/signup') }}"><span>Register your Organisation</span></a>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="chart_img">
									<img class="img-fluid"  style="width:50%" src="assets_landing_page/img/boy.png" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
						<div class="row recent_update_text align-items-center">
							<div class="col-lg-5">
								<div class="common_style">
									<p class="line">Executives Role </p>
									<h3>The system registration pages will guide you through a smart profile process from beginning to finish. </h3>
									<p>Save time! Relax and think that about 800 companies will see your profile… Some documentations will be required for uploading as the candidate conclude the registration.</p>
									<a class="primary_btn" href="#"><span>Sign Up Now</span></a>
								</div>
							</div>
							<div class="col-lg-6 text-right">
								<div class="chart_img">
									<img class="img-fluid" style="width:50%" src="assets_landing_page/img/girl.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================ Recent Update Area =================-->

	<!--================Start Big Features Area =================-->
	<section class="section_gap big_features">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-8 text-center">
					<div class="main_title">
						<p class="top_title">Features Specifications</p>
						<h2>Amazing Features That makes us Awesome and different!</h2>
						<p>Our Exec-Portal cuts down recruitment timing and costs and provides EMPLOYERS with already interviewed, vetted and rated candidates ready to be appointed just after the Company/Organisationhas gone through the Interview video and profile</p>
					</div>
				</div>
			</div>
			<div class="row features_content">
				<div class="col-lg-4 offset-lg-1">
					<div class="big_f_left">
						<img class="img-fluid" src="assets_landing_page/img/f-img1.png" alt="">
					</div>
				</div>
				<div class="col-lg-4 offset-lg-2">
					<div class="common_style">
						<p class="line">We are more than a Job Portal</p>
						<h3>limitless monitoring</h3>
						<p style="font-size:18px">Hiring of a candidate is not the end, there will be monitoring mechanism or scoring in terms of achievemments or awards which build up your profile for next Exec-Portal Job Application</p>
						<a class="primary_btn" href="{{ url('/signup') }}"><span>Learn More</span></a>
					</div>
				</div>
				<div class="border-line"></div>
				<img class="shape1"src="assets_landing_page/img/shape1.png" alt="">
				<img class="shape2"src="assets_landing_page/img/shape2.png" alt="">
				<img class="shape3"src="assets_landing_page/img/shape1.png" alt="">
			</div>

			<div class="row features_content bottom-features">
				<div class="col-lg-5">
					<div class="common_style">
						<p class="line">for a change</p>
						<h3>Organisations appky to you(executive)</h3>
						<p style="font-size:18px"> If an Organisation would like you to work for them , they can apply to you and Vise Versa</p>
						<a class="primary_btn" href="{{ url('/signin') }}"><span>Learn More</span></a>
					</div>
				</div>
				<div class="col-lg-5 offset-lg-2">
					<div class="big_f_left">
						<img class="img-fluid" src="assets_landing_page/img/f-img2.png" alt="">
					</div>
				</div>
				<div class="border-line"></div>
				<img class="shape1"src="assets_landing_page/img/shape1.png" alt="">
				<img class="shape2"src="assets_landing_page/img/shape2.png" alt="">
				<img class="shape3"src="assets_landing_page/img/shape1.png" alt="">
			</div>
		</div>
	</section>
	<!--================End Big Features Area =================-->


	<!--================Impress Area =================-->
	<section class="impress_area">
		<div class="container">
			<div class="impress_inner">
				<h2>Got Impressed by our features?</h2>
				<a class="primary_btn" href="{{ url('/signup') }}"><span> Open an Account as an Organisation or Executive now</span></a>
			</div>
		</div>
	</section>
	<!--================End Impress Area =================-->


	<!--================Footer Area =================-->
	<footer class="footer_area">
		<div class="container">
			<div class="row footer_inner">
				<div class="col-lg-5 col-sm-6">
					<aside class="f_widget ab_widget">
						<div class="f_title">
							<h3>About Me</h3>
						</div>
						<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | System made by <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://ecowebplus.com" target="_blank">EcoWebPlus</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
					</aside>
				</div>
				<div class="col-lg-5 col-sm-6">
					<aside class="f_widget news_widget">
						<div class="f_title">
							<h3>Newsletter</h3>
						</div>
						<p>Stay updated with our latest trends</p>
						<div id="mc_embed_signup">
							<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
							 method="get" class="subscribe_form relative">
								<div class="input-group d-flex flex-row">
									<input name="EMAIL" placeholder="Enter email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '"
									 required="" type="email">
									<button class="btn sub-btn"><span class="lnr lnr-arrow-right"></span></button>
								</div>
								<div class="mt-10 info"></div>
							</form>
						</div>
					</aside>
				</div>

			</div>
		</div>
	</footer>
	<!--================End Footer Area =================-->

	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="assets_landing_page/js/jquery-3.2.1.min.js"></script>
	<script src="assets_landing_page/js/popper.js"></script>
	<script src="assets_landing_page/js/bootstrap.min.js"></script>
	<script src="assets_landing_page/js/stellar.js"></script>
	<script src="assets_landing_page/js/jquery.magnific-popup.min.js"></script>
	<script src="assets_landing_page/vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="assets_landing_page/vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="assets_landing_page/vendors/isotope/isotope-min.js"></script>
	<script src="assets_landing_page/vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="assets_landing_page/js/jquery.ajaxchimp.min.js"></script>
	<script src="assets_landing_page/vendors/counter-up/jquery.waypoints.min.js"></script>
	<script src="assets_landing_page/vendors/counter-up/jquery.counterup.min.js"></script>
	<script src="/assets_landing_pagejs/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="assets_landing_page/js/gmaps.min.js"></script>
	<script src="assets_landing_page/js/theme.js"></script>
</body>

</html>

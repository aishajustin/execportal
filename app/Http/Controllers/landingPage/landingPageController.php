<?php

namespace App\Http\Controllers\landingPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class landingPageController extends Controller
{
    //
    public function viewOrganisationInformation(){
        return view ('pages.landingPage.organisations');

      }

      public function viewExecutiveInformation(){
          return view ('pages.landingPage.executives');

        }


        public function viewContactUs(){
            return view ('pages.landingPage.contactUs');

          }


            public function signin(){
                return view ('pages.landingPage.login');

              }

              public function signup(){
                  return view ('pages.landingPage.signup');

                }








}

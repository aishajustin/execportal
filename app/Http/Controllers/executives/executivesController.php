<?php

namespace App\Http\Controllers\executives;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class executivesController extends Controller
{
    //
    public function viewExecutivesIndex(){
        return view ('pages.executivesIndex');

      }




      public function profile(){
          return view ('pages.executives.profile');

        }

        public function viewProfile(){
            return view ('pages.executives.profile');

          }

          public function viewInterviews(){
              return view ('pages.executives.interviews.viewInterviews');

            }
            public function completedInterviews(){
                return view ('pages.executives.interviews.completedInterviews');

              }

              public function viewJobs(){
                  return view ('pages.executives.jobs.viewJobs');

                }

                public function myApplications(){
                    return view ('pages.executives.jobs.myApplications');

                  }


}

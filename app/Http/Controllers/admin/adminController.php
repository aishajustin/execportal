<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class adminController extends Controller
{
    //

    public function viewAdminIndex(){
        return view ('pages.adminIndex');

      }


      public function viewOrganisations(){
          return view ('pages.admin.organisations.viewOrganisations');

        }


        public function addOrganisation(){
            return view ('pages.admin.organisations.addOrganisation');

          }

          public function viewExecutives(){
              return view ('pages.admin.executives.viewExecutives');

            }

            public function listAllInterviews(){
                return view ('pages.admin.interviews.listAllInterviews');

              }

              public function viewApplications(){
                  return view ('pages.admin.executives.viewApplications');

                }


                public function profile(){
                    return view ('pages.admin.profile');

                  }

}

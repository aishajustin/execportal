<?php

namespace App\Http\Controllers\organisations;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class organisationsController extends Controller
{
    //
    public function viewOrgsIndex(){
        return view ('pages.organisationsIndex');

      }

      public function orgProfile(){
          return view ('pages.organisations.orgProfile');

        }

      public function postJob(){
          return view ('pages.organisations.jobs.postJob');

        }

        public function viewJobsPost(){
            return view ('pages.organisations.jobs.viewJobsPost');

          }

          public function findExecutives(){
              return view ('pages.organisations.jobs.findExecutives');

            }

            public function listInterviews(){
                return view ('pages.organisations.interviews.listInterviews');

              }

              public function completedInterviews(){
                  return view ('pages.organisations.interviews.completedInterviews');

                }

                public function pendingInterviews(){
                    return view ('pages.organisations.interviews.pendingInterviews');

                  }


                  public function viewApplications(){
                      return view ('pages.organisations.applications.viewApplications');

                    }
                    public function requestedProfiles(){
                        return view ('pages.organisations.applications.requestedProfiles');

                      }
}

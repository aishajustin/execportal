<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


// Dashboard  routes
Route::get('/adminIndex','admin\adminController@viewAdminIndex');
Route::get('/executivesIndex','executives\executivesController@viewExecutivesIndex');
Route::get('/organisationsIndex','organisations\organisationsController@viewOrgsIndex');


// Admin Routes
Route::get('/viewOrganisations','admin\adminController@viewOrganisations');
Route::get('/viewExecutives','admin\adminController@viewExecutives');
Route::get('/viewApplications','admin\adminController@viewApplications');
Route::get('/addOrganisation','admin\adminController@addOrganisation');
Route::get('/listAllInterviews','admin\adminController@listAllInterviews');
Route::get('/profile','admin\adminController@profile');



// Executive Routes
Route::get('/profile','executives\executivesController@profile');
Route::get('/completedInterviews','executives\executivesController@completedInterviews');
Route::get('/viewInterviews','executives\executivesController@viewInterviews');
Route::get('/myApplications','executives\executivesController@myApplications');
Route::get('/viewJobs','executives\executivesController@viewJobs');


// Organisation Routes
Route::get('/requestedProfiles','organisations\organisationsController@requestedProfiles');
Route::get('/viewApplications','organisations\organisationsController@viewApplications');
Route::get('/completedInterviews','organisations\organisationsController@completedInterviews');
Route::get('/pendingInterviews','organisations\organisationsController@pendingInterviews');
Route::get('/listInterviews','organisations\organisationsController@listInterviews');
Route::get('/findExecutives','organisations\organisationsController@findExecutives');
Route::get('/postJob','organisations\organisationsController@postJob');
Route::get('/viewJobsPost','organisations\organisationsController@viewJobsPost');
Route::get('/orgProfile','organisations\organisationsController@orgProfile');


// Landing pages
Route::get('/org','landingPage\landingPageController@viewOrganisationInformation');
Route::get('/exe','landingPage\landingPageController@viewExecutiveInformation');
Route::get('/contact','landingPage\landingPageController@viewContactUs');
Route::get('/signin','landingPage\landingPageController@signin');
Route::get('/signup','landingPage\landingPageController@signup');
